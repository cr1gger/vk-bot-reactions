const express = require('express');
const bodyParser = require('body-parser');
const VkBot = require('node-vk-bot-api');
const beseda_id = 2000000002;
const app = express();
const randomId = (peerId = 0) => {
    return parseInt(new Date().getTime() + '' + peerId + '' + Math.floor(Math.random() * 10000), 10)
}
const bot = new VkBot({
    token: '931427453e09381b2b9d8b6a8a40d68a6081b63d7290b0721e424c173b12eb35b5be53918ff014ea5c424',
    confirmation: '24b35a58',
});

bot.event('message_new',(ctx) => {
    if (ctx.message.peer_id !== 2000000001 && ctx.message.peer_id !== 2000000002) {
        bot.execute('messages.send', {
            peer_id: beseda_id,
            message: 'Новое сообщение в ЛС: https://vk.com/gim206396147',
            random_id: randomId()
        })
    }
    console.log(ctx)
});
bot.event('group_leave', (ctx) => {
    const user_id = ctx.message.user_id
    bot.execute('users.get', {
        user_ids: user_id
    }).then((result) => {
        bot.execute('messages.send', {
            peer_id: beseda_id,
            message: `💩${result[0].first_name} ${result[0].last_name}(@id${user_id}) покинул группу 💩`,
            random_id: randomId()
        })
    })
})
bot.event('group_join', (ctx) => {
    const user_id = ctx.message.user_id
    bot.execute('users.get', {
        user_ids: user_id
    }).then((result) => {
        bot.execute('messages.send', {
            peer_id: beseda_id,
            message: `✔ ${result[0].first_name} ${result[0].last_name}(@id${user_id}) вступил в группу! 👍`,
            random_id: randomId()
        })
    })
})


app.use(bodyParser.json());

app.post('/', bot.webhookCallback);

app.listen(5000);